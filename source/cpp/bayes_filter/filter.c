float transition_model(int to, int from, int control);
float observation_model(int observarion, int state);

void filter(float* b, int n_states, int z, int u) {
  // clear the states
  float b_pred[n_states];
  for(int i=0; i<n_states; i++)
    b_pred[i]=0;

  // predict
  for(int i=0; i<n_states; i++)
    for(int j=0; j<n_states; j++)
      b_pred[i]+=transition_model(i,j,u)*b[j];

  // integrate the observation
  float inverse_eta = 0;
  for (int i=0; i<n_states; i++)
    inverse_eta += b_pred[i]*=observation_model(z,i);

  // normalize
  float eta = 1./inverse_eta;
  for (int i=0; i<n_states; i++)
    b[i] = b_pred[i] * eta;
}
